const { request } = require('express');
const express = require('express');
const { parse } = require('ipaddr.js');
const router = express.Router();
const mongo = require('../../mongo');

router.post('/delete', async (req, res) => {
    let db1 = mongo.get().collection("USER_TABLE");
    let del = db1.deleteOne({ "_id": req.body._id });
    console.log("deleted");
    res.json({ status: "deleted" });
});

router.post('/search', async (req, res) => {
    let db1 = mongo.get().collection("USER_TABLE");

    let search = await db1.find({ "_id": req.body._id }).toArray()
    res.json({ "search": search });
});

router.post('/update', async (req, res) => {

    let db1 = mongo.get().collection("USER_TABLE");
    let flag = 0;

    var validation_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var validation_phoneno = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (req.body._id === "") {
        flag = 1;
        return false;
    }
    else if (req.body.user_name == "") {
        flag = 1;
        return false;
    }
    else if (req.body.first_name == "") {
        flag = 1;
        return false;
    }
    else if (req.body.last_name == "") {
        flag = 1;
        return false;
    }
    else if (req.body.email_id == "" || !req.body.email_id.toLowerCase().match(validation_email)) {
        flag = 1;
        return false;
    }

    else if (req.body.phone_no == "" || !validation_phoneno.test(req.body.phone_no)) {
        flag = 1;
        return false;
    }
    else if (req.body.role == "") {
        flag = 1;
        return false;
    }
    else if (req.body.college == "") {
        flag = 1;
        return false;
    }
    else if (req.body.department == "") {
        flag = 1;
        return false;
    }
    else if (req.body.year_batch == "") {
        flag = 1;
        return false;
    }
    else if (flag === 0) {
        var Id = req.body._id;
        let json = {
            "USER_ID": req.body.user_name,
            "FIRST_NAME": req.body.first_name.toUpperCase().trim(),
            "LAST_NAME": req.body.last_name.toUpperCase().trim(),
            "EMAIL_ID": req.body.email_id,
            "PHONE_NO": parseInt(req.body.phone_no),
            "ROLE": req.body.role,
            "COLLEGE": req.body.college.toUpperCase().trim(),
            "DEPARTMENT": req.body.department.toUpperCase().trim(),
            "YEAR_BATCH": parseInt(req.body.year_batch)
        }


        let update = db1.updateOne({ '_id': Id }, { $set: json });
        res.json({ status: 1 })
    }

});

router.post('/category', async (req, res) => {
    let db1 = mongo.get().collection("CATEGORY_TABLE");
    let max = await db1.find().sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    else {
        new_id = 1;
    }
    let json = {
        "_id": parseInt(new_id),
        "CATEGORY_ID": parseInt(new_id),
        "CATEGORY_NAME": req.body.category_name,
        "CATEGORY_TAG": req.body.tag_array
    }
    let user = await db1.insertOne(json);

    if (user.acknowledged) {
        res.json({ status: 1 });
    }
    else {
        res.json({ status: 0 });
    }
});

router.post('/topic', async (req, res) => {
    let db1 = mongo.get().collection("TOPIC_TABLE");
    let max = await db1.find().sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    else {
        new_id = 1;
    }
    let json = {
        "_id": parseInt(new_id),
        "TOPIC_ID": parseInt(new_id),
        "TOPIC_NAME": req.body.topic_name,
        "CATEGORY_ID": parseInt(req.body.category_id),
        "CATEGORY_NAME": req.body.category_name,
        "TOPIC_TAG": req.body.tag_array
    }
    let user = await db1.insertOne(json);

    if (user.acknowledged) {
        res.json({ status: 1 });
    }
    else {
        res.json({ status: 0 });
    }
});

router.post('/add_question', async (req, res) => {
    let db1 = mongo.get().collection("QUESTION_TABLE");
    let max = await db1.find().sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    else {
        new_id = 1;
    }

    if (req.body.answer === 1 || req.body.answer === 2 || req.body.answer === 3 || req.body.answer === 4) {
        let json = {
            "_id": parseInt(new_id),
            "QUESTION_ID": parseInt(new_id),
            "TOPIC_ID": parseInt(req.body.topic_id),
            "CATEGORY_ID": parseInt(req.body.category_id),
            "QUESTION_DESCRIPTION": req.body.question_description,
            "OPTION_1": req.body.option_1,
            "OPTION_2": req.body.option_2,
            "OPTION_3": req.body.option_3,
            "OPTION_4": req.body.option_4,
            "ANSWER": parseInt(req.body.answer),
            "LEVEL": parseInt(req.body.level),
            "EXPLANATION": req.body.explanation,
            "QUESTION_TAG": req.body.tag_array
        }
        let user = await db1.insertOne(json);

        if (user.acknowledged) {
            res.json({ status: 1 });
        }
        else {
            res.json({ status: 0 });
        }

    }
    else {
        res.json({ status: "pick ans 1,2,3,4" });
    }
});

router.post('/update_question', async (req, res) => {
    let db1 = mongo.get().collection("QUESTION_TABLE");
    let flag = 0;
    if (req.body._id === "") {
        flag = 1;
        return false;
    }
    else if (req.body.topic_id === "") {
        flag = 1;
        return false;
    }
    else if (req.body.category_id === "") {
        flag = 1;
        return false;
    }
    else if (req.body.question_description === "") {
        flag = 1;
        return false;
    }
    else if (req.body.option_1 === "") {
        flag = 1;
        return false;
    }

    else if (req.body.option_2 === "") {
        flag = 1;
        return false;
    }
    else if (req.body.option_3 === "") {
        flag = 1;
        return false;
    }
    else if (req.body.option_4 == "") {
        flag = 1;
        return false;
    }
    else if (req.body.answer === "") {
        flag = 1;
        return false;
    }
    else if (req.body.explanation === "") {
        flag = 1;
        return false;
    }
    else if (req.body.tag_array === "") {
        flag = 1;
        return false;
    }
    else if (flag === 0) {
        if (req.body.answer === 1 || req.body.answer === 2 || req.body.answer === 3 || req.body.answer === 4) {
            var Id = req.body._id;
            let json = {
                "TOPIC_ID": parseInt(req.body.topic_id),
                "CATEGORY_ID": parseInt(req.body.category_id),
                "QUESTION_DESCRIPTION": req.body.question_description,
                "OPTION_1": req.body.option_1,
                "OPTION_2": req.body.option_2,
                "OPTION_3": req.body.option_3,
                "OPTION_4": req.body.option_4,
                "ANSWER": parseInt(req.body.answer),
                "EXPLANATION": req.body.explanation,
                "QUESTION_TAG": req.body.tag_array
            }


            let update = db1.updateOne({ '_id': Id }, { $set: json });
            res.json({ status: 1 })
        }
        else {
            res.json({ status: "pick ans 1,2,3,4" });
        }
    }

});







router.post('/submission_get_answers', async (req, res) => {
    let db1 = mongo.get().collection("QUESTION_TABLE");
    let status = [];
    for (let i = 0; i < req.body.submission.length; i++) {
        let element = req.body.submission[i];
        let search = await db1.find({ "QUESTION_ID": parseInt(element.question_id) }).toArray()
        console.log(search[0].ANSWER);
        if (element.submission_answer === search[0].ANSWER) {
            status.push({ question_id: element.question_id, status: 1, answer: search[0].ANSWER })
        }
        else {
            status.push({ question_id: element.question_id, status: 0, answer: search[0].ANSWER })
        }

    }
    res.json(status);

});



module.exports = router;