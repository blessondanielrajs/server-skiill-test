const express = require('express');
const router = express.Router();
const mongo = require('../../mongo');
const MD5 = require('crypto-js/md5');
const multer = require('multer'); //Its use for uploading
const path = require('path');
const fs = require('fs');
var parse = require('csv-parse');// Excel format Use for bulk uploading





router.post('/single_register', async (req, res) => {

    let db1 = mongo.get().collection("USER_TABLE");
    let flag = 0;

    var validation_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var validation_phoneno = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (req.body._id === "") {
        flag = 1;
        return false;
    }
    else if (req.body.user_name === "") {
        flag = 1;
        return false;
    }
    else if (req.body.first_name === "") {
        flag = 1;
        return false;
    }
    else if (req.body.last_name === "") {
        flag = 1;
        return false;
    }
    else if (req.body.email_id === "" || !req.body.email_id.toLowerCase().match(validation_email)) {
        flag = 1;
        return false;
    }

    else if (req.body.phone_no === "" || !validation_phoneno.test(req.body.phone_no)) {
        flag = 1;
        return false;
    }
    else if (req.body.role === "") {
        flag = 1;
        return false;
    }
    else if (req.body.college === "") {
        flag = 1;
        return false;
    }
    else if (req.body.department === "") {
        flag = 1;
        return false;
    }
    else if (req.body.year_batch === "") {
        flag = 1;
        return false;
    }
    else if (flag === 0) {
        
        let json = {
            "_id": req.body._id,
            "USER_ID": req.body.user_name,
            "FIRST_NAME": req.body.first_name.toUpperCase().trim(),
            "LAST_NAME": req.body.last_name.toUpperCase().trim(),
            "EMAIL_ID": req.body.email_id,
            "PHONE_NO": parseInt(req.body.phone_no),
            "ROLE": req.body.role,
            "COLLEGE": req.body.college.toUpperCase().trim(),
            "DEPARTMENT": req.body.department.toUpperCase().trim(),
            "YEAR_BATCH": parseInt(req.body.year_batch),
            "PASSWORD": MD5(req.body._id).toString()
        }


        let insert = await db1.insertOne(json);
        if (insert.acknowledged) {
            res.json({ status: 1 });
        }
        else {
            res.json({ status: 0 });
        }
    }

});

const imageStorage = multer.diskStorage({
    destination: 'temp/',
    destination: async function (req, file, cb) {
        let path = 'temp/';
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path, { recursive: true }, (err) => {
                if (err) throw err;
            });
        }
        await cb(null, path);
    },
    filename: (req, file, cb) => {
        cb(null, "file" + path.extname(file.originalname))
    }

});

const imageUpload = multer({
    storage: imageStorage,
    limits: {
        fileSize: 1000000 // 1000000 Bytes = 1 MB
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(csv)$/)) {
            return cb(new Error('Please upload a CSV'))
        }
        cb(undefined, true);
    }
})

router.post('/bulk', imageUpload.single('file'), async (req, res) => {
    var csvData = []; let status = [], key = 0;
    let path = "temp/file.csv";
    let db1 = mongo.get().collection("USER_TABLE")
    await fs.createReadStream(path)
        .pipe(parse({ delimiter: ',' }))
        .on('data', async function (csvrow) {
            csvData.push(csvrow);
        })
        .on('end', async function () {
            for (let i = 1; i < csvData.length; i++) {


                let json = {
                    "_id": csvData[i][1],
                    "USER_ID": csvData[i][2],
                    "FIRST_NAME": csvData[i][3].toUpperCase().trim(),
                    "LAST_NAME": csvData[i][4].toUpperCase().trim(),
                    "EMAIL_ID": csvData[i][5],
                    "PHONE_NO": parseInt(csvData[i][6]),
                    "ROLE": csvData[i][7],
                    "COLLEGE": csvData[i][8].toUpperCase().trim(),
                    "DEPARTMENT": csvData[i][9].toUpperCase().trim(),
                    "YEAR_BATCH": parseInt(csvData[i][10])
                }


                var validation_email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var validation_phoneno = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

                if (json._id === "") {
                    status.push({ json: json._id, addstatus: 0, msg: "Enter Id", "key": ++key });
                }
                else if (json.USER_ID === "") {
                    status.push({ json: json.role, addstatus: 0, msg: "Enter USER ID", "key": ++key });
                }
                else if (json.FIRST_NAME === "") {
                    status.push({ json: password, addstatus: 0, msg: "Enter FIRST NAME", "key": ++key });
                }
                else if (json.LAST_NAME === "") {
                    status.push({ json: password, addstatus: 0, msg: "Enter LAST NAME", "key": ++key });
                }
                else if (json.EMAIL_ID === "" || !json.EMAIL_ID.toLowerCase().match(validation_email)) {
                    status.push({ json: password, addstatus: 0, msg: "Invalid EMAIL ID", "key": ++key });
                }
                else if (json.PHONE_NO === "" || !validation_phoneno.test(json.PHONE_NO)) {
                    status.push({ json: password, addstatus: 0, msg: "Invalid PHONE NO", "key": ++key });
                }
                else if (json.ROLE === "") {
                    status.push({ json: password, addstatus: 0, msg: "Enter ROLE", "key": ++key });
                }
                else if (json.COLLEGE === "") {
                    status.push({ json: password, addstatus: 0, msg: "Enter COLLEGE", "key": ++key });
                }
                else if (json.DEPARTMENT === "") {
                    status.push({ json: password, addstatus: 0, msg: "Enter DEPARTMENT", "key": ++key });
                }
                else if (json.YEAR_BATCH === "") {
                    status.push({ json: password, addstatus: 0, msg: "Enter YEAR BATCH", "key": ++key });
                }
                else {
                    let user = await db1.insertOne(json);
                    if (user.acknowledged) {
                        status.push({ json: json.USER_ID, addstatus: 1, msg: "Successfully Added", "key": ++key });
                    }
                }
            }
            res.json({ Status: 1, addStatus: status });
        });

}, (error, req, res, next) => {
    res.status(400).send({ error: error.message });
});

router.post('/reset_password', async (req, res) => {
    let db1 = mongo.get().collection("USER_TABLE");
    let json=
    {
        'PASSWORD': MD5(req.new_password).toString()
    }
    let update = db1.updateOne({ '_id': req.body._id, 'PASSWORD': req.body.old_password }, { $set:json});
    res.json({ status: "success" })

});

module.exports = router;