const express = require('express');
const router = express.Router();


const Admin = require('./admin');
const Admin_operation = require('./Admin/operation');
const Student_operation = require('./Student/operation');



router.use('/admin', Admin);
router.use('/admin/operation', Admin_operation);
router.use('/student/operation', Student_operation);


module.exports = router;