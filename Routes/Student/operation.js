const express = require('express');
const router = express.Router();
const mongo = require('../../mongo');

router.post('/getcategory', async (req, res) => {
    let db1 = mongo.get().collection("CATEGORY_TABLE");
    let find_category = await db1.find().toArray()
    res.json({ "result": find_category, "Status": 1 });
});

router.post('/gettopic', async (req, res) => {
    console.log(req.body);
    let db1 = mongo.get().collection("TOPIC_TABLE");
    let find_topic = await db1.find({ "CATEGORY_ID": parseInt(req.body.CATEGORY_ID) }).toArray()
    res.json({ "result": find_topic, "Status": 1 });
});
router.post('/gettotalcount', async (req, res) => {
    console.log(req.body);
    let db1 = mongo.get().collection("QUESTION_TABLE");
    let total_question = await db1.find({ "CATEGORY_ID": parseInt(req.body.CATEGORY_ID), "TOPIC_ID": parseInt(req.body.TOPIC_ID), "LEVEL": parseInt(req.body.LEVEL) }).toArray()
    res.json({ "result": parseInt(total_question.length), "Status": 1 });
});

router.post('/getquestions', async (req, res) => {
    console.log(req.body);
    let db1 = mongo.get().collection("QUESTION_TABLE");
    let db2 = mongo.get().collection("STATUS_TABLE");
    var question_array = [], query;
    let flag = 0;

    let max = await db2.find().sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;

    let json = {
        "_id": new_id,
        "TEST_ID": new_id,
        "USER_ID": parseInt(req.body.USER_ID),
        "USER_NAME": 'JACOB',
        "TIMESTAMP": parseInt(req.body.TIMESTAMP),
        "CATEGORY_ID": parseInt(req.body.CATEGORY_ID),
        "TOPIC_ID": parseInt(req.body.TOPIC_ID),
        "LEVEL": parseInt(req.body.LEVEL),
        "QUESTION_COUNT": parseInt(req.body.QUESTION_COUNT),
        "SCORE": parseInt(req.body.SCORE),
        "SUBMISSION": parseInt(req.body.SUBMISSION)
    }
    let status = await db2.insertOne(json);


    if (status.acknowledged) {

        query = {
            "TOPIC_ID": parseInt(req.body.TOPIC_ID),
            "LEVEL": parseInt(req.body.LEVEL)
        }
        let random = await db1.aggregate([
            { $match: query }, // filter the results
            { $project: { "ANSWER": 0, "EXPLANATION": 0 } },
            { $sample: { size: parseInt(req.body.QUESTION_COUNT) } }
        ]).toArray();
        console.log(random);
        await question_array.push(random);

        res.json(question_array);
    }
    else {
        res.json({ "Status": 0 });
    }


});


router.post('/submission', async (req, res) => {
    //console.log(req.body.TEST_ID);
    let db1 = mongo.get().collection("QUESTION_TABLE");
    let db2 = mongo.get().collection("STATUS_TABLE");
    let status = [], score = 0;
    for (let i = 0; i < req.body.SUBMISSION.length; i++) {
        let element = req.body.SUBMISSION[i];
        let search = await db1.find({ "QUESTION_ID": parseInt(element.QUESTION_ID), "ANSWER": parseInt(element.SUBMISSION_ANSWER) }).toArray()
        // console.log(search);
        if (search.length) {

            status.push({ question_id: element.QUESTION_ID, status: 1 })
        }
        else {
            status.push({ question_id: element.QUESTION_ID, status: 0 })
        }
        if (status[i].status === 1) {
            score = score + 1;
           
        }

    }
    console.log(req.body.TEST_ID)
    let json = {
        "SCORE": parseInt(score)
    }
    let update = db2.updateOne({ 'TEST_ID': parseInt(req.body.TEST_ID) }, { $set: json });

    res.json(status);

});

module.exports = router;